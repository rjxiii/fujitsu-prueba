import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {

  selected = 1;
  records = [];
  documents = [
    {
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      sharedWith: 100,
      links: 5,
      downloable: true,
      uploable: true,
      expanded: true,
    },
    {
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento audio - MP3',
      status: 'Completado',
      downloable: true,
      uploable: false,
      expanded: false,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      downloable: true,
      uploable: true,
      expanded: false,
      links: 5,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      links: 5,
      downloable: true,
      uploable: true,
      expanded: false,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      sharedWith: 8,
      links: 5,
      downloable: true,
      uploable: true,
      expanded: false,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      sharedWith: 12,
      downloable: true,
      uploable: true,
      expanded: false,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      links: 5,
      downloable: true,
      uploable: true,
      expanded: false,
    },{
      createdAt: '00/00/0000 00:00',
      templateUsed: 'Nombre plantilla',
      source:
        'Departamento de origen de este documento, puede doblar las lineas que sean necesarias',
      sourceEvaluation: 'Evaluación',
      restrictions: ['Restricción 1', 'Restricción 2', 'Restricción 3'],
      tags: [
        'Etiqueta 1',
        'Etiqueta 2',
        'Etiqueta 3',
        'Etiqueta 4',
        'Etiqueta 5',
        'Etiqueta 6',
        'Etiqueta 7',
        'Etiqueta 8',
      ],
      name: 'Nombre del documento máximo 2 líneas',
      description:
        'A - 1 | Descripcion del documento  que debe doblar como máximo 2 líneas',
      type: 'Documento texto - PDF',
      status: 'Completado',
      sharedWith: 99,
      downloable: true,
      uploable: true,
      expanded: false,
    },
  ];

  constructor() {}

  ngOnInit(): void {  }
  ngAfterViewInit() {
    const inner = window.innerHeight;
    const viewport = document.getElementById('viewport');
    const viewport_y = viewport?.getBoundingClientRect().y;
    if (viewport && viewport_y ){
      viewport.style.height = `${inner - viewport_y}px`;
    }
  }

  expandAll() {
    this.documents.map((d) => {
      d.expanded = true;
    });
  }
  deExpandAll() {
    this.documents.map((d) => {
      d.expanded = false;
    });
  }
  isAllExpanded(data:any){
    return this.documents.find((d:any)=>d.expanded === false)
  }
}
