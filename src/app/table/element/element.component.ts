import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.scss'],
})
export class ElementComponent implements OnInit {
  @Input() document: any;

  constructor() {}

  ngOnInit(): void {}

  sharedClick() {
    console.log('Shared Click');
  }
  linksClick() {
    console.log('Links Click');
  }
  uploadClick() {
    console.log('Upload Click');
  }
  downloadClick() {
    console.log('Download Click');
  }

  returnTags(elements: any) {
    if (elements.length > 5) {
      return {
        text: elements.filter((d: any, i: any) => i < 5).join(', '),
        others: elements.filter((d: any, i: any) => i >= 5),
      };
    }
    return { text: elements.join(', ') };
  }
}
